CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Counter field formatter is a simple module which animates number countups for a
specific field (float/decimal/integer).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/counter_field_format

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/counter_field_format


REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------
 * Navigate to Administration > Extend and enable the module.

 Instructions For Opening Image Fields In Popup:

  * Navigate to Administration > Structure > Content types > Content Type
    > Manage Display and change the format of the image field to
    "Counter Field Format".
  * Click on the settings icon and provide the values for duration and
    easing style.


MAINTAINERS
-----------

* sravya challa (sravya_12) - https://www.drupal.org/u/sravya_12
